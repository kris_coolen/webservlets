package be.kriscoolen.hoofdstuk4.opdracht5.service;

public class BMIService {

    public float calculateBMI(float height, float weight){
        return weight/(height*height);
    }
}
