package be.kriscoolen.hoofdstuk4.opdracht5.controller;

import be.kriscoolen.hoofdstuk4.opdracht4.domain.AddressBean;
import be.kriscoolen.hoofdstuk4.opdracht4.service.AddressService;
import be.kriscoolen.hoofdstuk4.opdracht4.service.AddressServiceImpl;
import be.kriscoolen.hoofdstuk4.opdracht5.service.BMIService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/bmiMVC")
public class BMIServlet extends HttpServlet {
    private BMIService service = new BMIService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/pages/BMI.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        float height = Float.parseFloat(req.getParameter("height"));
        float weight = Float.parseFloat(req.getParameter("weight"));
        float bmi = service.calculateBMI(height,weight);
        //req.setAttribute("bmi",bmi); //attribute only exists in request scope
        session.setAttribute("bmi",bmi); //attribute exists in session scope
        //req.getRequestDispatcher("/WEB-INF/pages/BMI.jsp").forward(req,resp); //forward request
        resp.sendRedirect(req.getRequestURI()); //redirect instead of forward -> form info will not be resend at refresh page
    }
}
