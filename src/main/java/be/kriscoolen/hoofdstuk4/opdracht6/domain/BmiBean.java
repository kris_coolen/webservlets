package be.kriscoolen.hoofdstuk4.opdracht6.domain;

import java.io.Serializable;

public class BmiBean implements Serializable {

    float weight;
    float height;
    float bmi;

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getBmi() {
        return bmi;
    }

    public void setBmi(float bmi) {
        this.bmi = bmi;
    }
}
