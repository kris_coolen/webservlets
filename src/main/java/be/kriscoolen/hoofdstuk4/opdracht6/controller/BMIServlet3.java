package be.kriscoolen.hoofdstuk4.opdracht6.controller;

import be.kriscoolen.hoofdstuk4.opdracht6.domain.BmiBean;
import be.kriscoolen.hoofdstuk4.opdracht6.service.BMIService2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/bmiMVC3")
public class BMIServlet3 extends HttpServlet {
    private BMIService2 service = new BMIService2();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/pages/BMI3.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BmiBean bmiBean = new BmiBean();
        bmiBean.setHeight(Float.parseFloat(req.getParameter("height")));
        bmiBean.setWeight(Float.parseFloat(req.getParameter("weight")));
        float bmi = service.calculateBMI(bmiBean.getHeight(), bmiBean.getWeight());
        bmiBean.setBmi(bmi);
        req.getSession().setAttribute("bmiBean", bmiBean);
        //req.setAttribute("bmi",bmi);
        resp.sendRedirect(req.getRequestURI());
        //req.getRequestDispatcher("/WEB-INF/pages/BMI2.jsp").forward(req, resp);
    }
}
