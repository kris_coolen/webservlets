package be.kriscoolen.hoofdstuk4.opdracht6.controller;

import be.kriscoolen.hoofdstuk4.opdracht6.service.BMIService2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/bmiMVC2")
public class BMIServlet2 extends HttpServlet {
    private BMIService2 service = new BMIService2();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/pages/BMI2.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        float height = Float.parseFloat(req.getParameter("height"));
        float weight = Float.parseFloat(req.getParameter("weight"));
        float bmi = service.calculateBMI(height,weight);
        session.setAttribute("bmi",bmi);
        //req.setAttribute("bmi",bmi);
        //resp.sendRedirect(req.getRequestURI());
        req.getRequestDispatcher("/WEB-INF/pages/BMI2.jsp").forward(req,resp);
    }
}
