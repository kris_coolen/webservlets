package be.kriscoolen.hoofdstuk4.opdracht4.controller;

import be.kriscoolen.hoofdstuk4.opdracht4.domain.AddressBean;
import be.kriscoolen.hoofdstuk4.opdracht4.service.AddressService;
import be.kriscoolen.hoofdstuk4.opdracht4.service.AddressServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addressMVC")
public class AddressServlet extends HttpServlet {

    private AddressService service = new AddressServiceImpl();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getSession().getAttribute("addressBean")!=null){
            req.getRequestDispatcher("/WEB-INF/pages/AddressResult.jsp").forward(req,resp);
        }
        else {
            req.getRequestDispatcher("/WEB-INF/pages/AddressForm.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AddressBean ab = new AddressBean();
        ab.setName(req.getParameter("name"));
        ab.setFirstName(req.getParameter("firstName"));
        ab.setStreet(req.getParameter("street"));
        ab.setNumber(req.getParameter("number"));
        ab.setZipCode(req.getParameter("zipCode"));
        ab.setCity(req.getParameter("city"));
        ab.setCountry(req.getParameter("country"));
        ab.setTelephoneNumber(req.getParameter("telephoneNumber"));
        ab.setEmail(req.getParameter("email"));
        service.registerAddress(ab);
        //req.setAttribute("addressBean",ab); //request scope
        req.getSession().setAttribute("addressBean",ab); //session scope
        req.getRequestDispatcher("/WEB-INF/pages/AddressResult.jsp").forward(req,resp);
    }
}
