package be.kriscoolen.hoofdstuk4.opdracht4.service;

import be.kriscoolen.hoofdstuk4.opdracht4.domain.AddressBean;

public interface AddressService {
    public void registerAddress(AddressBean address);

}
