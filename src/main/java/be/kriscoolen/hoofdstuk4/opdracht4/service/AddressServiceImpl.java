package be.kriscoolen.hoofdstuk4.opdracht4.service;

import be.kriscoolen.hoofdstuk4.opdracht4.domain.AddressBean;

public class AddressServiceImpl implements AddressService {

    @Override
    public void registerAddress(AddressBean address) {
        System.out.println(address);
    }
}
