package be.kriscoolen.common.guestBook;

import javax.net.ssl.SSLException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GuestBookDao {
    private Connection connection=null;
    public GuestBookDao(String driver, String url, String user, String password){
        try{
            Class.forName(driver);
            this.connection = DriverManager.getConnection(url,user,password);
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return this.connection;
    }

    private ResultSet runSql(String sql) throws SQLException{
        Statement statement = connection.createStatement();
        return statement.executeQuery(sql);
    }

    public List<GuestBookBean> getGuestBookItems(){
        List<GuestBookBean> result = new ArrayList<>();
        ResultSet resultSet;
        try{
            resultSet = runSql("SELECT * FROM GuestBook");
            while(resultSet.next()){
                LocalDate date = resultSet.getDate(2).toLocalDate();
                String name = resultSet.getString(3);
                String message = resultSet.getString(4);
                GuestBookBean guestBookBean = new GuestBookBean(date,name,message);
                result.add(guestBookBean);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        if(result.size()>0) return result;
        else return null;
    }

    public void addGuestBookItem(GuestBookBean item){
        try{
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO GuestBook (Date, Name, Message) VALUES (?,?,?)");

            stmt.setDate(1,Date.valueOf(item.getDate()));
            stmt.setString(2,item.getName());
            stmt.setString(3,item.getMessage());
            if(stmt.executeUpdate()==0){
                System.out.println("error: item could not be added to guestbook");
            }
            else{
                System.out.println("item successfully added to Table GuestBook");
            }

        }catch (SQLException se){
            System.out.println("oops : " + se);
        }
    }

    public void clearGuestBook(){
        try {
            connection.createStatement().executeUpdate("TRUNCATE GuestBook");
        }
        catch(SQLException se){
            System.out.println("oops : " + se);
        }
    }
}
