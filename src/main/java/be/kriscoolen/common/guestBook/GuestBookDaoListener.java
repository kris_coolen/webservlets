package be.kriscoolen.common.guestBook;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class GuestBookDaoListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();
        String driver = sc.getInitParameter("driver");
        String url = sc.getInitParameter("url");
        String user = sc.getInitParameter("user");
        String password = sc.getInitParameter("password");
        GuestBookDao guestBookDao = new GuestBookDao(driver,url,user,password);
        sc.setAttribute("guestBookRepository",guestBookDao);
    }
}
