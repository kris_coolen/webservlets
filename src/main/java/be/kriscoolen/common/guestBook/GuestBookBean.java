package be.kriscoolen.common.guestBook;

import java.time.LocalDate;

public class GuestBookBean {
    private LocalDate date;
    private String name;
    private String message;

    public GuestBookBean(LocalDate date, String name, String message) {
        this.date = date;
        this.name = name;
        this.message = message;
    }

    public GuestBookBean(String name, String message) {
        this(LocalDate.now(),name,message);
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
