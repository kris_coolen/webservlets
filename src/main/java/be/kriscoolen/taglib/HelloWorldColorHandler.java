package be.kriscoolen.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HelloWorldColorHandler extends SimpleTagSupport implements DynamicAttributes {

    private Map<String,String> atts = new HashMap<>();

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        out.print("<span ");
        for(String att: atts.keySet()){
            out.print(att+"='" + atts.get(att)+"'");
        }
        out.print(">");
        out.print("Hello World");
        out.print("</span>");
    }

    @Override
    public void setDynamicAttribute(String uri, String name, Object value) throws JspException {
        if(uri==null){
            //only attributes from default namespace
            atts.put(name,(String) value);
        }
    }

}
