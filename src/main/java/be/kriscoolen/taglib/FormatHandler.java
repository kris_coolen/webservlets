package be.kriscoolen.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FormatHandler extends SimpleTagSupport implements DynamicAttributes {

    private Map<String,String> attributes = new HashMap<>();

    @Override
    public void setDynamicAttribute(String uri, String name, Object value) throws JspException {
        if(uri==null){
            //only attributes from default namespace
            attributes.put(name,(String) value);
        }
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        out.print("<div ");
        for(String attribute: attributes.keySet()){
            out.print(attribute+"='" + attributes.get(attribute)+"'");
        }
        out.print(">");
        getJspBody().invoke(out);
        out.print("</div>");
    }
}
