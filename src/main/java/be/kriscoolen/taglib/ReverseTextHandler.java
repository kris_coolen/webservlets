package be.kriscoolen.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class ReverseTextHandler extends SimpleTagSupport {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        if(text!=null && !text.isBlank()) {
            out.print("Result of reversing text '" + text + "': " + new StringBuilder(text).reverse().toString());
        }
    }
}
