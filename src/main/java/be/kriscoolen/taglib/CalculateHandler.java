package be.kriscoolen.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class CalculateHandler extends SimpleTagSupport {

    private int number1;
    private int number2;

    public void setNumber1(int value){
        number1=value;
    }

    public int getNumber1(){
        return number1;
    }

    public void setNumber2(int value){
        number2 = value;
    }

    public int getNumber2(){
        return number2;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        out.print(number1*number2);
    }
}
