package be.kriscoolen.hoofdstuk3.opdracht13;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.Optional;
import java.util.stream.Stream;

@WebServlet("/NameFormWithCookie")
public class NameFormWithCookieServlet extends HttpServlet {
    public static final String NAME="name";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        String firstName ="";
        String lastName = "";
        String name = null; //format firstname_lastname
        //Get firstName and lastname from cookie "name"
        Cookie[] cookies = req.getCookies();
        if(cookies !=null){
            Optional<String> optionalName = Stream.of(cookies).filter(c->c.getName().equals(NAME)).map(Cookie::getValue).findAny();
            if(optionalName.isPresent()){
                name = URLDecoder.decode(optionalName.get(),"UTF-8");
                String[] names = name.split("_");
                if(names.length==2) {
                    firstName = names[0];
                    lastName = names[1];
                }
            }
        }
        try(PrintWriter out = resp.getWriter()){
            out.println("<html><header><title> Registration form with cookies </title>");
            out.println("</header><body><form method='POST'>");
            out.println("first name: <input type='text' name='PARAMETER_FIRSTNAME' value='"+firstName+"'/>");
            out.println("last name: <input type='text' name='PARAMETER_LASTNAME' value='"+lastName+"'/>");
            out.println("<input type='submit' value='Submit'/>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("PARAMETER_FIRSTNAME");
        String lastName = req.getParameter("PARAMETER_LASTNAME");
        if(firstName.length()>0 && lastName.length()>0){
            Cookie cookie = new Cookie(NAME,firstName+"_"+lastName);
            cookie.setMaxAge(60*60);
            resp.addCookie(cookie);
            try(PrintWriter out = resp.getWriter()){
                out.println("<html><header><title> Registration validation </title>");
                out.println("<body><h1> welcome "+firstName+" "+lastName+"</h1>");
                out.println("<a href='NameFormWithCookie'>Back to form</a>");
                out.println("</body></html>");
            }
        }
        else{
            try(PrintWriter out = resp.getWriter()) {
                out.println("<html><header><title> Registration validation </title>");
                out.println("<body><h1> please enter a first name and a last name</h1>");
                out.println("<a href='NameFormWithCookie'>Back to form</a></body></html>");
            }
        }

    }
}
