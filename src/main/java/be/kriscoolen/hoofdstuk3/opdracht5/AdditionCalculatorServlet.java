package be.kriscoolen.hoofdstuk3.opdracht5;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;

@WebServlet("/calculator/addition")
public class AdditionCalculatorServlet extends HttpServlet {

    private final String RESULT = "AdditionCalculatorServlet.result";
    private final String NUMBER = "number";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Object resultAttribute = session.getAttribute(RESULT);
        int result = 0;
        if(resultAttribute!=null){
            result = (Integer) resultAttribute;
        }

        String message = "";
        Object messageAttribute = req.getAttribute("message");
        if(messageAttribute != null){
            message = (String) messageAttribute;
        }

        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        try(PrintWriter out = resp.getWriter()){
            out.println("<!DOCTYPE html>");
            out.println("<html><head><title>Addition Calculator");
            out.println("</title></head><body>");
            out.print("<form method='POST'>");
            out.print(message + "<br />");
            out.println("Result: " + result + "<br/>");
            out.println("<input type='text' name='number' /><br/>");
            out.println("<input type='submit' value='Add' />");
            out.println("</form></body></html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
       int result = 0;
       String message="";
       HttpSession session = req.getSession();
       Object resultAttribute = session.getAttribute(RESULT);
       if(resultAttribute!=null){
           result = (Integer) resultAttribute;
       }
       String numberParameter = req.getParameter(NUMBER);
       if(numberParameter != null){
           try{
               result+=Integer.parseInt(numberParameter);
           } catch(NumberFormatException ex){
               message = "Invalid number";
           }
       }
       req.setAttribute("message", message);
       session.setAttribute(RESULT,result);
       doGet(req,resp);
    }
}
