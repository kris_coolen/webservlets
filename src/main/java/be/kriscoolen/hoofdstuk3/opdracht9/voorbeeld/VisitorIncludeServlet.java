package be.kriscoolen.hoofdstuk3.opdracht9.voorbeeld;

import be.kriscoolen.hoofdstuk3.opdracht7.visitor.VisitorSessionListener2;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="VisitorIncludeServlet",value="/VisitorInclude")
public class VisitorIncludeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int total = (Integer) getServletContext().getAttribute(VisitorSessionListener2.TOTAL);
        int active = (Integer) getServletContext().getAttribute(VisitorSessionListener2.ACTIVE);
        @SuppressWarnings("resource")
        PrintWriter out = resp.getWriter();
        out.println("Total visitors" + total + "<br/>");
        out.println("Current Visitors" + active + "<br/>");
    }
}
