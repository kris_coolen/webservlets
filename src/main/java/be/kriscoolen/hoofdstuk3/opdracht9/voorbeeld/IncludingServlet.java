package be.kriscoolen.hoofdstuk3.opdracht9.voorbeeld;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Including")
public class IncludingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,  IOException {
        req.getSession();
        resp.setContentType("text/html");
        try(PrintWriter out = resp.getWriter()){
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Including Servlet</title></head><body>");
            out.println("<div>Including another servlet</div>");
            RequestDispatcher dispatcher = getServletContext().getNamedDispatcher("VisitorIncludeServlet");
            dispatcher.include(req,resp);
            out.println("</body></html>");
        }
    }
}
