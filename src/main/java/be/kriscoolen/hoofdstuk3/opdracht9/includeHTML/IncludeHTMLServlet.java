package be.kriscoolen.hoofdstuk3.opdracht9.includeHTML;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/IncludeHTML")
public class IncludeHTMLServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        try(PrintWriter out= resp.getWriter()){
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Included HTML page through server</title></head><body>");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/pages/IncludedHTML.html");
            dispatcher.include(req,resp);
            out.println("</body></html>");
        }


    }
}
