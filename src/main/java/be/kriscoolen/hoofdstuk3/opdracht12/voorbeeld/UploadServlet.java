package be.kriscoolen.hoofdstuk3.opdracht12.voorbeeld;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Upload")
@MultipartConfig(location="C:\\temp",fileSizeThreshold=1024*1024,
        maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)
public class UploadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part part = req.getPart("uploadfile");
        part.write("uploadedfile");
        resp.setContentType("text/html");
        try(PrintWriter out = resp.getWriter()){
            out.println("<html><head><title>Upload Servlet</title></head><body>");
            out.println("File uploaded <br />");
            out.println("</body></html>");
        }
    }
}
