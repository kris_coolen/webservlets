package be.kriscoolen.hoofdstuk3.opdracht12.image;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

@WebServlet("/UploadImage")
@MultipartConfig
public class UploadImageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/UploadImageFile.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part part = req.getPart("uploadImage");
        InputStream inputStream = part.getInputStream();
        final byte[] bytes;
        try(inputStream){
            bytes=inputStream.readAllBytes();
        }
        resp.setContentType("image/jpeg");
        resp.setContentLength(bytes.length);
        resp.getOutputStream().write(bytes);
    }
}
