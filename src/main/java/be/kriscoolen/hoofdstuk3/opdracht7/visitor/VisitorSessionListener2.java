package be.kriscoolen.hoofdstuk3.opdracht7.visitor;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class VisitorSessionListener2 implements HttpSessionListener, ServletContextListener {
    public static final String TOTAL = "visitorsTotal2";
    public static final String ACTIVE = "visitorsActive2";
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        ServletContext sc = se.getSession().getServletContext();

        Integer total = (Integer) sc.getAttribute(TOTAL);
        sc.setAttribute(TOTAL,total+1);

        Integer active = (Integer) sc.getAttribute(ACTIVE);
        sc.setAttribute(ACTIVE,active+1);
    }
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        ServletContext sc = se.getSession().getServletContext();
        Integer active = (Integer) sc.getAttribute(ACTIVE);
        sc.setAttribute(ACTIVE,active-1);
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(ACTIVE,0);
        sce.getServletContext().setAttribute(TOTAL,0);
    }
}
