package be.kriscoolen.hoofdstuk3.opdracht7.visitor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Visitors2")
public class VisitorServlet2 extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        request.getSession(); // in order to invoke the weblistener class VisitorSessionListener
        int total = (Integer) getServletContext().getAttribute(VisitorSessionListener2.TOTAL);
        int active = (Integer) getServletContext().getAttribute(VisitorSessionListener2.ACTIVE);
        request.setAttribute("totalVisitors2",total);
        request.setAttribute("activeVisitors2",active);
        request.getRequestDispatcher("WEB-INF/pages/Visitors2.jsp").forward(request,response);
    }
}
