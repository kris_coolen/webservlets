package be.kriscoolen.hoofdstuk3.opdracht2;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="GreetingServlet", value="/Greeting", initParams = @WebInitParam(name="welcomeMessage",value="Hallo gebruiker!"))
public class GreetingServlet extends HttpServlet {

    private String text;

    @Override
    public void init() throws ServletException{
        ServletContext context = getServletContext();
        context.log("init method started");
        text = getInitParameter("welcomeMessage");
        if(text==null) throw new ServletException("Parameter text not found");
        context.log("init method ended");
    }

    @Override
    public void destroy() {
        ServletContext context = getServletContext();
        context.log("destroy method started");
        super.destroy();
        context.log("destroy method ended");
    }

    /*@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        try(PrintWriter out = resp.getWriter()){
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Hello Mars Servlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(text);
            out.println("</body>");
            out.println("</html>");
        }
    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("welcomeMessage", text);
        req.getRequestDispatcher("/WEB-INF/pages/Greeting.jsp").forward(req, resp);
    }
}
