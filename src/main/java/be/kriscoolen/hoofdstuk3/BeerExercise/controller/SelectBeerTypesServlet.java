package be.kriscoolen.hoofdstuk3.BeerExercise.controller;

import be.kriscoolen.hoofdstuk3.BeerExercise.domain.Beer;
import be.kriscoolen.hoofdstuk3.BeerExercise.repository.BeerDAO;
import be.kriscoolen.hoofdstuk3.BeerExercise.service.BeerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = {"/beers/find/bytype"})
public class SelectBeerTypesServlet extends HttpServlet {

    private List<Beer> findBeersByRegExpInName(String regExp){
        BeerService bs = new BeerService(new BeerDAO());
        List<Beer> beers = bs.findBeerByRegExpInName(regExp);
        return beers;
    }

    /*@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        String[] beerTypes = req.getParameterValues("beerType");

        //blondBeers = getBeersByType(beerTypes[0]);
        // brownBeers = getBeersByType(beerTypes[1]);

        if(beerTypes==null){
            try(PrintWriter out = resp.getWriter()){
                out.println("<!DOCTYPE html>");
                out.print("<html><head><title>Beer list by selected type");
                out.print("</title></head><body>");
                out.print("You have no beers selected");
                out.print("</body></html>");
            }
            return;
        }
        if(beerTypes.length==1){
            List<Beer> beers = getBeersByType(beerTypes[0]);
            try(PrintWriter out = resp.getWriter()){
                out.print("<html><head><title>Beer list");
                out.print("</title></head><body>");
                out.print("<table style=width:100%");
                out.print("<tr>");
                out.print("<th>Brand</th><th>Alcohol</th>");
                out.print("</tr>");
                for(Beer b: beers){
                    out.print("<tr>");
                    out.print("<td>"+b.getBrand()+"</td>");
                    out.print("<td>"+b.getAlcoholPercentage()+"</td>");
                    out.print("</tr>");

                }
                out.print("</table></body></html>");
            }
            return;
        }
        if(beerTypes.length==2){
            List<Beer> blondBeers = getBeersByType(beerTypes[0]);
            List<Beer> brownBeers= getBeersByType(beerTypes[1]);
            try(PrintWriter out = resp.getWriter()){
                out.print("<html><head><title>Beer list");
                out.print("</title></head><body>");
                out.print("<table style=width:100%");
                out.print("<tr>");
                out.print("<th>Brand</th><th>Alcohol</th>");
                out.print("</tr>");
                for(Beer b: blondBeers){
                    out.print("<tr>");
                    out.print("<td>"+b.getBrand()+"</td>");
                    out.print("<td>"+b.getAlcoholPercentage()+"</td>");
                    out.print("</tr>");

                }
                out.print("<hr>");
                for(Beer b: brownBeers){
                    out.print("<tr>");
                    out.print("<td>"+b.getBrand()+"</td>");
                    out.print("<td>"+b.getAlcoholPercentage()+"</td>");
                    out.print("</tr>");

                }
                out.print("</table></body></html>");
            }
        }
    }*/

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] beerTypes = req.getParameterValues("beerType");
        String regExp=null;
        if(beerTypes!=null) {
            if (beerTypes.length == 1) regExp = beerTypes[0];
            else if (beerTypes.length == 2) regExp = beerTypes[0] + "|" + beerTypes[1];
        }
        List<Beer> beerList =  findBeersByRegExpInName(regExp);
        req.setAttribute("beerList",beerList);
        req.getRequestDispatcher("/WEB-INF/pages/BeerCatalog.jsp").forward(req,resp);
    }
}
