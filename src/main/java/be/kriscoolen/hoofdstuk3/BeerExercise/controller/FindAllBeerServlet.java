package be.kriscoolen.hoofdstuk3.BeerExercise.controller;

import be.kriscoolen.hoofdstuk3.BeerExercise.domain.Beer;
import be.kriscoolen.hoofdstuk3.BeerExercise.repository.BeerDAO;
import be.kriscoolen.hoofdstuk3.BeerExercise.service.BeerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = {"/beers/find/all"})
public class FindAllBeerServlet extends HttpServlet {

    private List<Beer> getBeers(){
        BeerService bs = new BeerService(new BeerDAO());
        List<Beer> beers = bs.findAllBeers();
        return beers;
    }

    /*@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        //BeerService bs = new BeerService(new BeerDAO());
        List<Beer> beers = getBeers();
        try(PrintWriter out = resp.getWriter()){
            out.print("<html><head><title>Beer list");
            out.print("</title></head><body>");
            out.print("<table style=width:100%");
            out.print("<tr>");
            out.print("<th>Brand</th><th>Alcohol</th>");
            out.print("</tr>");
            for(Beer b: beers){
                out.print("<tr>");
                out.print("<td>"+b.getBrand()+"</td>");
                out.print("<td>"+b.getAlcoholPercentage()+"</td>");
                out.print("</tr>");

            }
            out.print("</table></body></html>");
        }

    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Beer> beers = getBeers();
        req.setAttribute("beerList",beers);
        req.getRequestDispatcher("/WEB-INF/pages/BeerCatalog.jsp").forward(req,resp);

    }
}
