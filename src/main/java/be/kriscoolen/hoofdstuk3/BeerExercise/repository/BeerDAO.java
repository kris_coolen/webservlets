package be.kriscoolen.hoofdstuk3.BeerExercise.repository;


import be.kriscoolen.hoofdstuk3.BeerExercise.domain.Beer;
import be.kriscoolen.hoofdstuk3.BeerExercise.db.DriverManagerWrapper;

import java.sql.*;
import java.util.*;

public class BeerDAO {


    public  List<Beer> findAllBeers(){
        List<Beer> result=new ArrayList<>();
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try(Connection con= DriverManager.getConnection(DriverManagerWrapper.url, DriverManagerWrapper.login, DriverManagerWrapper.pwd);
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT Name,Alcohol FROM Beers"))
        {
            while(rs.next()) {
                //add the other beers
                String brand = rs.getString(1);
                double alcoholPercentage = rs.getFloat(2);
                Beer beer = new Beer(brand,alcoholPercentage);
                result.add(beer);
            }

        }catch(SQLException se){
            System.out.println("oops : " + se);
        }
        if(result.size()>0) return result;
        else return null;
    }

    public List<Beer> findBeerByRegExpInName(String regExp){
        if(regExp==null) return null;
        String sql = "SELECT Name,Alcohol FROM Beers where Name REGEXP '" + regExp +"'";
        List<Beer> result=new ArrayList<>();
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try(Connection con= DriverManager.getConnection(DriverManagerWrapper.url, DriverManagerWrapper.login, DriverManagerWrapper.pwd);
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery(sql))
        {
            while(rs.next()) {
                //add the other beers
                String brand = rs.getString(1);
                double alcoholPercentage = rs.getFloat(2);
                Beer beer = new Beer(brand,alcoholPercentage);
                result.add(beer);
            }

        }catch(SQLException se){
            System.out.println("oops : " + se);
        }
        if(result.size()>0) return result;
        else return null;
    }

    public static void main(String[] args) {
        BeerDAO beerDAO = new BeerDAO();
        List<Beer> beers = beerDAO.findAllBeers();
        System.out.println(beers.size());
    }
}
