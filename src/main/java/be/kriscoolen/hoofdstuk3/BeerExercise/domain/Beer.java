package be.kriscoolen.hoofdstuk3.BeerExercise.domain;

public class Beer {
private String Brand;
private double alcoholPercentage;

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public double getAlcoholPercentage() {
        return alcoholPercentage;
    }

    public void setAlcoholPercentage(double alcoholPercentage) {
        this.alcoholPercentage = alcoholPercentage;
    }

    public Beer(String brand, double alcoholPercentage) {
        Brand = brand;
        this.alcoholPercentage = alcoholPercentage;
    }
}
