package be.kriscoolen.hoofdstuk3.BeerExercise.service;

import be.kriscoolen.hoofdstuk3.BeerExercise.domain.Beer;
import be.kriscoolen.hoofdstuk3.BeerExercise.repository.BeerDAO;

import java.util.List;

public class BeerService {

    private BeerDAO beerDao;

    public BeerService(BeerDAO beerDao){
        this.beerDao=beerDao;
    }

    public  List<Beer> findAllBeers(){
        List<Beer> beers = this.beerDao.findAllBeers();
        return beers;
    }

    public List<Beer> findBeerByRegExpInName(String regExp){
        List<Beer> beers = this.beerDao.findBeerByRegExpInName(regExp);
        return beers;
    }

    public static void main(String[] args) {
        BeerService bs = new BeerService(new BeerDAO());
        System.out.println(bs.findAllBeers().size());
        System.out.println(bs.findAllBeers().get(0).getBrand());
    }

}
