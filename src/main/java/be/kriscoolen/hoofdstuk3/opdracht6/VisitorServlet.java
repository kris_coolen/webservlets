package be.kriscoolen.hoofdstuk3.opdracht6;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/Visitors")
public class VisitorServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        request.getSession(); // in order to invoke the weblistener class VisitorSessionListener
        int total = (Integer) getServletContext().getAttribute(VisitorSessionListener.TOTAL);
        int active = (Integer) getServletContext().getAttribute(VisitorSessionListener.ACTIVE);
        request.setAttribute("totalVisitors",total);
        request.setAttribute("activeVisitors",active);
        request.getRequestDispatcher("WEB-INF/pages/Visitors.jsp").forward(request,response);
    }
}
