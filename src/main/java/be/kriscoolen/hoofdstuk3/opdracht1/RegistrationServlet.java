package be.kriscoolen.hoofdstuk3.opdracht1;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
@WebServlet(urlPatterns = {"/registration","/Registration"})
public class RegistrationServlet extends HttpServlet {
    /*@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        resp.setContentType("text/html");
        //resp.setCharacterEncoding("UTF-8");
        try(PrintWriter out = resp.getWriter()){
            out.print("<html><head><title>Registration Servlet");
            out.print("</title></head><body>");
            out.print("<form method='POST'>");
            out.print("username:");
            out.print("<input type='text' name='username'>");
            out.print("<br>");
            out.print("<br>");
            out.print("password:");
            out.print("<input type='password' name='password'> ");
            out.print("<br>");
            out.print("<br>");
            out.print("submit:");
            out.print("<input type='submit' value='Submit'");
            out.print("</body></html>");
        }
    }*/
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/pages/RegistrationForm.jsp").forward(req,resp);
    }
    /*@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)  throws IOException {
        resp.setContentType("text/html");
        //resp.setCharacterEncoding("UTF-8");
        String userName = req.getParameter("username");
        String passWord = req.getParameter("password");
        try(PrintWriter out = resp.getWriter()){
            out.println("<!DOCTYPE html>");
            out.print("<html><head><title>Registration Servlet");
            out.print("</title></head><body>");
            out.print("Welcome " + userName);
            out.print("<br>");
            out.print("<br>");
            out.print("your password: " + passWord);
            out.print("</body></html>");
        }
    }*/

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
        String userName = req.getParameter("inputName");
        String passWord = req.getParameter("inputPassword");
        req.setAttribute("userName",userName);
        req.setAttribute("passWord",passWord);
        req.getRequestDispatcher("/WEB-INF/pages/RegistrationResult.jsp").forward(req,resp);
    }


}