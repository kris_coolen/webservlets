package be.kriscoolen.hoofdstuk3.opdracht1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="HelloMarsServlet", value="/HelloMars", initParams = @WebInitParam(name="text",value="Hello World"))
public class HelloMarsServlet extends HttpServlet {
    private String text;

    @Override
    public void init() throws ServletException {
        text = getInitParameter("text");
        if(text==null) throw new ServletException("Parameter text not found");
    }

   /* @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        resp.setContentType("text/html");
        try(PrintWriter out = resp.getWriter()){
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Hello Mars Servlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(text);
            out.println("</body>");
            out.println("</html>");
        }
    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException, ServletException {
        req.setAttribute("text",text);
        req.getRequestDispatcher("/WEB-INF/pages/HelloMars.jsp").forward(req,resp);
    }
}
