package be.kriscoolen.hoofdstuk3.opdracht1;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
//@WebServlet(urlPatterns = {"/Helloworld","/HelloWorld","/helloworld"})
public class HelloWorldServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.getSession();
        resp.setContentType("text/html");
        //resp.setCharacterEncoding("UTF-8");
        /*try(PrintWriter out = resp.getWriter()){
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Hello World Servlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("Hello World");
            out.println("</body>");
            out.println("</html>");
        }*/
        req.getRequestDispatcher("/WEB-INF/pages/ch3op1.jsp").forward(req,resp);
    }
}