package be.kriscoolen.hoofdstuk3.opdracht1;

import java.io.Serializable;
import java.util.Enumeration;

public class RequestBean implements Serializable {

    private String httpMethod;
    private String uri;
    private String url;
    private String servletPath;
    private String contextPath;
    private Enumeration<String> headerNames;

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getServletPath() {
        return servletPath;
    }

    public void setServletPath(String servletPath) {
        this.servletPath = servletPath;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public Enumeration<String> getHeaderNames() {
        return headerNames;
    }

    public void setHeaderNames(Enumeration<String> headerNames) {
        this.headerNames = headerNames;
    }
}
