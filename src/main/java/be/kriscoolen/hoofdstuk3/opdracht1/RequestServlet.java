package be.kriscoolen.hoofdstuk3.opdracht1;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
@WebServlet(name="RequestServlet", value="/Request")
public class RequestServlet extends HttpServlet {
   /* @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
       // resp.setCharacterEncoding("UTF-8");
        try(PrintWriter out = resp.getWriter()){
            out.println("http method:" + req.getMethod() + "<br>");
            out.println("request URI: " + req.getRequestURI() + "<br>");
            out.println("request URL: " + req.getRequestURL() + "<br>");
            out.println("servlet path: " +req.getServletPath() + "<br>");
            out.println("context path: " +req.getContextPath()+ "<br>");
            out.println("headernames: " +req.getHeaderNames()+ "<br>");
        }
    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       RequestBean requestBean = new RequestBean();
       requestBean.setHttpMethod(req.getMethod());
       requestBean.setUri(req.getRequestURI());
       requestBean.setUrl(req.getRequestURL().toString());
       requestBean.setServletPath(req.getServletPath());
       requestBean.setContextPath(req.getContextPath());
       requestBean.setHeaderNames(req.getHeaderNames());
       req.setAttribute("requestBean",requestBean);
       req.getRequestDispatcher("/WEB-INF/pages/Request.jsp").forward(req,resp);

    }
}