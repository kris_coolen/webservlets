package be.kriscoolen.hoofdstuk3.opdracht10;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/userAge")
public class SelectUserAgeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String page = "WEB-INF/pages/SelectUserAge.jsp";
        String ageString = req.getParameter("PARAMETER_AGE");
        req.setAttribute("ATTRIBUTE_AGE",ageString);
        if(ageString!=null){
            int age;
            try{
                age = Integer.parseInt(ageString);
                if(age<0){
                    page = "WEB-INF/pages/InvalidAge.jsp";
                }
                else if(age<10){
                    page = "WEB-INF/pages/Child.jsp";
                }
                else if (age<20){
                    page = "WEB-INF/pages/Teenager.jsp";
                }
                else{
                    page = "WEB-INF/pages/Adult.jsp";
                }
            } catch(NumberFormatException ex){
                page = "WEB-INF/pages/InvalidAge.jsp";
            }
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher(page);
        dispatcher.forward(req,resp);
    }
}
