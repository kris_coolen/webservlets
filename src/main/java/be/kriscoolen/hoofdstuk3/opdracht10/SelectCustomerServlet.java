package be.kriscoolen.hoofdstuk3.opdracht10;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/SelectCustomer")
public class SelectCustomerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String page = "WEB-INF/pages/SelectConsumer.jsp";
        String consumption = req.getParameter("consumption");
        req.setAttribute("consumptionAttribute",consumption);
        if(consumption!=null){
            int cons;
            try{
                cons = Integer.parseInt(consumption);
                if(cons>2000){
                    page = "WEB-INF/pages/BigConsumer.jsp";
                }
                else if(cons>=0){
                    page = "WEB-INF/pages/SmallConsumer.jsp";
                }
                else{
                    page = "WEB-INF/pages/Invalid.jsp";
                }
            } catch(NumberFormatException ex){
                page = "WEB-INF/pages/Invalid.jsp";
            }
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher(page);
        dispatcher.forward(req,resp);
    }
}
