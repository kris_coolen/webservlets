package be.kriscoolen.hoofdstuk3.opdracht14;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@WebFilter(urlPatterns = "/*")
public class LogFilter extends HttpFilter {
    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {
        LocalDateTime now = LocalDateTime.now();
        String message = String.format("%s %s %s\n",now.toString(),req.getRequestURL(),req.getRemoteHost());
        req.getServletContext().log(message);
        chain.doFilter(req,resp);
    }
}
