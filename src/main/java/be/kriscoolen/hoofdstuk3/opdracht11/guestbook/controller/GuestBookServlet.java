package be.kriscoolen.hoofdstuk3.opdracht11.guestbook.controller;

import be.kriscoolen.common.guestBook.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/guestbook3")
public class GuestBookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        GuestBookDao guestBookDao = (GuestBookDao) getServletContext().getAttribute("guestBookRepository");
        List<GuestBookBean> guestBook = guestBookDao.getGuestBookItems();
        try(PrintWriter out = resp.getWriter()){
            out.print("<html><head><title>Guest book with listener");
            out.print("</title></head><body>");
            out.print("<table style=width:100%");
            out.print("<tr>");
            out.print("<th>Date</th><th>Name</th><th>Message</th>");
            out.print("</tr>");
            if(guestBook!=null) {
                for (GuestBookBean item : guestBook) {
                    out.print("<tr>");
                    out.print("<td>" + item.getDate() + "</td>");
                    out.print("<td>" + item.getName() + "</td>");
                    out.print("<td>" + item.getMessage() + "</td>");
                    out.print("</tr>");
                }
            }
            out.print("</table>");
            out.print("<hr>");
            out.print("<form method='POST'>");
            out.print("<table style=width:100%");
            out.print("<tr>");
            out.print("<td> name:</td>");
            out.print("<td> <input type='text' name='name'> </td>");
            out.print("<tr>");
            out.print("<td> message:</td>");
            out.print("<td> <input type='text' name='message'> </td>");
            out.print("<tr>");
            out.print("<td><input type='submit' name='add' value='add record'></td>");
            out.print("<td><input type='submit' name='delete' value = 'empty guestbook'></td>");
            out.print("</body></html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        GuestBookDao guestBookDao = (GuestBookDao) getServletContext().getAttribute("guestBookRepository");
        if(req.getParameter("add")!=null) {
            String name = req.getParameter("name");
            String message = req.getParameter("message");
            GuestBookBean newRecord = new GuestBookBean(name, message);
            guestBookDao.addGuestBookItem(newRecord);
        }
        else if(req.getParameter("delete")!=null){
            guestBookDao.clearGuestBook();
        }
        //doGet(req,resp);
        resp.sendRedirect(req.getRequestURI());
    }

}
