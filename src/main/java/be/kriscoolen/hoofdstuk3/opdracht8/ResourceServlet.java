package be.kriscoolen.hoofdstuk3.opdracht8;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

@WebServlet("/resource")
public class ResourceServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        InputStream in = getServletContext().getResourceAsStream("WEB-INF/mariadb.properties");
        Properties props = new Properties();
        props.load(in);
        in.close();
        /*response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        try(PrintWriter out = response.getWriter()){
            out.println("<!DOCTYPE html>");
            out.println("<html><head><title>");
            out.println("Resource Servlet");
            out.println("</title></head>");
            out.println("<body><h1>mariadb.properties</h1>");
            out.println(props.getProperty("driver")+"<br>");
            out.println(props.getProperty("url")+"<br>");
            out.println(props.getProperty("user")+"<br>");
            out.println(props.getProperty("password")+"<br>");
            out.println("</body></html>");*/
        // }
        request.setAttribute("propMariaDB",props);
        request.getRequestDispatcher("WEB-INF/pages/ShowMariaDBProperties.jsp").forward(request,response);

    }
}
