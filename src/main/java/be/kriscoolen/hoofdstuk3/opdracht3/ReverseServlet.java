package be.kriscoolen.hoofdstuk3.opdracht3;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/reverse")
public class ReverseServlet extends HttpServlet {

    /*@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  throws IOException {
        //UTF-8 already set by web.xml for both req en resp!
        String text = req.getParameter("text");
        String reversedText = new StringBuilder(text).reverse().toString();
        resp.setContentType("text/html");
        try(PrintWriter out = resp.getWriter()){
            out.println("<!DOCTYPE html>");
            out.println("<html><head><title>Reverse Servlet");
            out.println("</title></head><body>");
            out.println("the result of reversing '" + text + "' is : <br>" + reversedText);
            out.println("</body></html>");
        }
    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
        //UTF-8 already set by web.xml for both req en resp!
        String text = req.getParameter("text");
        String reversedText = new StringBuilder(text).reverse().toString();
        req.setAttribute("text", text);
        req.setAttribute("reversedText", reversedText);
        req.getRequestDispatcher("Reverse.jsp").forward(req, resp);
    }


}
