package be.kriscoolen.hoofdstuk3.opdracht3;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/square")
public class SquareServlet extends HttpServlet {

    /*@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  throws IOException {
        //UTF-8 already set by web.xml for both req en resp!
        String numberString = req.getParameter("nb");
        int number = 0;
        try{
            number = Integer.parseInt(numberString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        resp.setContentType("text/html");
        try(PrintWriter out = resp.getWriter()){
            out.println("<!DOCTYPE html>");
            out.println("<html><head><title>Reverse Servlet");
            out.println("</title></head><body>");
            out.println("The square of " + number + " is : " + number*number);
            out.println("</body></html>");
        }
    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
        //UTF-8 already set by web.xml for both req en resp!
        String numberString = req.getParameter("nb");
        int number = 0;
        try{
            number = Integer.parseInt(numberString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        req.setAttribute("square",number*number );
        req.getRequestDispatcher("Square.jsp").forward(req, resp);
    }

}
