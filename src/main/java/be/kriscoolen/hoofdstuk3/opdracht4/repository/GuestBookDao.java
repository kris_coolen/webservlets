package be.kriscoolen.hoofdstuk3.opdracht4.repository;

import be.kriscoolen.common.guestBook.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GuestBookDao {

    private String driver;
    private String url;
    private String user;
    private String password;

    public List<GuestBookBean> getGuestBookItems(){
        List<GuestBookBean> result = new ArrayList<>();
        loadDriver();
        try(Connection con= DriverManager.getConnection(url,user,password);
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM GuestBook"))
        {
            while(rs.next()){
                LocalDate date = rs.getDate(2).toLocalDate();
                String name = rs.getString(3);
                String message = rs.getString(4);
                GuestBookBean guestBookBean = new GuestBookBean(date,name,message);
                result.add(guestBookBean);
            }

        }catch (SQLException se){
            System.out.println("oops : " + se);
        }
        if(result.size()>0) return result;
        else return null;
    }

    public void addGuestBookItem(GuestBookBean item){
        loadDriver();
        try(Connection con= DriverManager.getConnection(url,user,password);
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO GuestBook (Date, Name, Message) VALUES (?,?,?)"))
        {
            stmt.setDate(1,Date.valueOf(item.getDate()));
            stmt.setString(2,item.getName());
            stmt.setString(3,item.getMessage());
            if(stmt.executeUpdate()==0){
                System.out.println("error: item could not be added to guestbook");
            }
            else{
                System.out.println("item successfully added to Table GuestBook");
            }

        }catch (SQLException se){
            System.out.println("oops : " + se);
        }
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void loadDriver(){
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
