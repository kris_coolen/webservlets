package be.kriscoolen.hoofdstuk3.opdracht4.controller;

import be.kriscoolen.common.guestBook.GuestBookBean;
import be.kriscoolen.hoofdstuk3.opdracht4.repository.GuestBookDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(
        value = "/guestbook",
        initParams= {@WebInitParam(name = "driver", value = "org.mariadb.jdbc.Driver"),
                     @WebInitParam(name="url", value="jdbc:mariadb://javaeegenk.be/javaeegenkDB1"),
                     @WebInitParam(name="user",value="javaeegenk"),
                     @WebInitParam(name="password",value="java%%g%nk2019")
        }
)
public class GuestBookServlet extends HttpServlet {

    private GuestBookDao guestBookDao = new GuestBookDao();

    @Override
    public void init() throws ServletException {
        String driver = getInitParameter("driver");
        if(driver==null) {
            throw new ServletException("Parameter driver not found");
        }
        else{
            guestBookDao.setDriver(driver);
        }
        String url = getInitParameter("url");
        if(url==null){
            throw new ServletException("Parameter url not found");
        }
        else{
            guestBookDao.setUrl(url);
        }
        String user = getInitParameter("user");
        if(user==null){
            throw new ServletException("Parameter user not found");
        }
        else{
            guestBookDao.setUser(user);
        }
        String password = getInitParameter("password");
        if(password==null){
            throw new ServletException("Parameter password not found");
        }
        else{
            guestBookDao.setPassword(password);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        List<GuestBookBean> guestBook = guestBookDao.getGuestBookItems();
        try(PrintWriter out = resp.getWriter()){
            out.print("<html><head><title>Guest book");
            out.print("</title></head><body>");
            out.print("<table style=width:100%");
            out.print("<tr>");
            out.print("<th>Date</th><th>Name</th><th>Message</th>");
            out.print("</tr>");
            for(GuestBookBean item: guestBook){
                out.print("<tr>");
                out.print("<td>"+item.getDate()+"</td>");
                out.print("<td>"+item.getName()+"</td>");
                out.print("<td>"+item.getMessage()+"</td>");
                out.print("</tr>");
            }
            out.print("</table>");
            out.print("<hr>");
            out.print("<form method='POST'>");
            out.print("<table style=width:100%");
            out.print("<tr>");
            out.print("<td> name:</td>");
            out.print("<td> <input required type='text' name='name'> </td>");
            out.print("<tr>");
            out.print("<td> message:</td>");
            out.print("<td> <input required type='text' name='message'> </td>");
            out.print("<tr>");
            out.print("<td><input type='submit' value='add record'></td>");
            out.print("</body></html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("name");
        String message = req.getParameter("message");
        GuestBookBean newRecord = new GuestBookBean(name,message);
        guestBookDao.addGuestBookItem(newRecord);
        doGet(req,resp);
    }
}
