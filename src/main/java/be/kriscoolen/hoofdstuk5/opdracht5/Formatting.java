package be.kriscoolen.hoofdstuk5.opdracht5;

public class Formatting {

    public static String round(float number, int precision){
        String formatter = "%."+precision+"f";
        return String.format(formatter,number);
    }
}
