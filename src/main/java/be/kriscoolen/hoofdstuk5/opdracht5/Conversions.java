package be.kriscoolen.hoofdstuk5.opdracht5;

public class Conversions {

    public static float kw2hp(float kw){
        return kw*1.34f;
    }

    public static float km2mile(float km){
        return km*0.621371f;
    }
}
