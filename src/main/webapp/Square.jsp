<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 19/03/2020
  Time: 22:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<html>
<head>
    <title>Square</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<main>
    <form method="GET" action="square">
        Enter the number that you wish to square:<br>
        <input type="number" name="nb"/>
        <input type="submit" value="compute square"/>
    </form>
    result: ${square}
</main>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>