<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 19/03/2020
  Time: 21:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Reverse</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<main>
    <form method="GET" action="reverse">
        Enter the text that you wish to reverse:<br>
        <input type="text" name="text"/>
        <input type="submit" value="Reverse"/>
    </form>
    Reversed text: ${reversedText}
</main>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>
