<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<div class="mt-5 container-fluid">
    <div class="row justify-content-center">
        <div class="col-auto">
            <h1>Chapter 3: Servlets</h1>
            <table class="table-striped">
                <tbody>
                <tr>
                    <td>opdracht 1</td>
                    <td><a href='HelloWorld'>Hello World</a></td>
                    <td>opdracht 7 a</td>
                    <td><a href='Visitors2'>visitors2</a></td>
                </tr>
                <tr>
                    <td>Voorbeeld p 31</td>
                    <td><a href='HelloMars'>Hello Mars</a></td>
                    <td>opdracht 7 b</td>
                    <td><a href='guestbook2'>Guestbook revisited</a></td>
                </tr>
                <tr>
                    <td>opdracht 2</td>
                    <td><a href='Greeting'>Greeting</a></td>
                    <td>opdracht 8</td>
                    <td><a href='resource'>read from mariadb.properties</a></td>
                </tr>
                <tr>
                    <td>opdracht 3 a</td>
                    <td><a href='Reverse.jsp'>Reverse</a></td>
                    <td>opdracht 9a</td>
                    <td><a href='Including'>include</a></td>
                </tr>
                <tr>
                    <td>opdracht 3 b</td>
                    <td><a href='Square.jsp'>Square</a></td>
                    <td>opdracht 9b</td>
                    <td><a href='IncludeHTML'>include an HTML</a></td>
                </tr>
                <tr>
                    <td>opdracht 4</td>
                    <td><a href='guestbook'>Guest book</a></td>
                    <td>opdracht 10 a</td>
                    <td><a href='SelectCustomer'>Select consumer</a></td>
                </tr>
                <tr>
                    <td>extra opdracht</td>
                    <td><a href='Request'>Show Request Info</a></td>
                    <td>opdracht 10 b</td>
                    <td><a href='userAge'>Select user age</a></td>
                </tr>
                <tr>
                    <td>extra opdracht</td>
                    <td><a href='Registration'>Register</a></td>
                    <td>opdracht 11</td>
                    <td><a href="guestbook3">Guestbook redirect</a></td>
                </tr>
                <tr>
                    <td>extra opdracht</td>
                    <td><a href='BeerIndex.jsp'>Beer Database</a></td>
                    <td>opdracht 12</td>
                    <td><a href='UploadFile.html'>upload file</a></td>
                </tr>
                <tr>
                    <td>opdracht 5 a</td>
                    <td><a href='calculator/addition'>Addition Calculator</a></td>
                    <td>opdracht 12</td>
                    <td><a href='UploadImage'>upload image to browser</a></td>
                </tr>
                <tr>
                    <td>opdracht 5 b</td>
                    <td><a href='calculator'>Calculator</a></td>
                    <td>opdracht 13</td>
                    <td><a href='NameFormWithCookie'>Form with cookie</a></td>
                </tr>
                <tr>
                    <td>opdracht 5 c</td>
                    <td><a href='calculator_URL_rewriting'>Calculator URL Rewriting</a></td>
                    <td>opdracht 16</td>
                    <td><a href='Secret.jsp'>secret page</a></td>
                </tr>
                <tr>
                    <td>opdracht 6</td>
                    <td><a href='Visitors'>visitors</a></td>
                    <td>opdracht 17</td>
                    <td><a href='guestbook4'>guestbook secured</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>