<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 25/03/2020
  Time: 13:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Secret page</title>
    <%@ include file="/common/jsps/head.jspf" %>
</head>
<body>
<h1>Top secret</h1>
<div class="mt2 container-fluid">
    This page contains highly sensitive information that is very well hidden for you.
</div>
</body>
</html>
