<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 26/03/2020
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Error page</title>
    <%@ include file="/common/jsps/head.jspf" %>
</head>
<body>
<c:url var="url" value="/"/>
<div class="error-container">
    <div class="error-content">
        <h2>Invalid user name or password.</h2>
        <img src="common/image/banner_error_404.jpg" style="height:60%";/>
        <p> Click here to go <a href="${url}">back</a></p>
    </div>
</div>
</body>
</html>
