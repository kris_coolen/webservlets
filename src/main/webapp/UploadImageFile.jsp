<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 24/03/2020
  Time: 19:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Upload JPEG Image</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<form method="post" action="UploadImage" enctype="multipart/form-data">
    Select file to upload:
    <input type="file" name="uploadImage" accept=".jpg"/></br>
    <input class="btn-primary" type="submit" value="Show in browser"/>
</form>
</body>
</html>
