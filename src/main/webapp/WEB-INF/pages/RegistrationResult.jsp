<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 20/03/2020
  Time: 10:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration Result</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<main>
    <h1>Welcome ${userName}, you are successfully registered!</h1>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h3>Registration data</h3>
                <table class="table-striped">
                    <thead>
                    <tr>
                        <td>User Name</td>
                        <td>Password</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>${userName}</td>
                        <td>${passWord}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>
