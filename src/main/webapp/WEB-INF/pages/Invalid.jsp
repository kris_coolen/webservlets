<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 24/03/2020
  Time: 9:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Invalid</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
Your input '${consumptionAttribute}' is invalid. Please enter a non-negative number.
</body>
</html>
