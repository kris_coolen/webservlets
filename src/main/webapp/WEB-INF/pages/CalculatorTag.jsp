<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 29/03/2020
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/mytaglib.tld" prefix="mtl"%>
<html>
<head>
    <title>Tag with attributes</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
    <mtl:calculate number1="3" number2="5"/>
</body>
</html>
