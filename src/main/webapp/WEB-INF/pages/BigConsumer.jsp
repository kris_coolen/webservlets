<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 24/03/2020
  Time: 9:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Big Consumer</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
You are a big consumer: Your yearly energy consumption is ${consumptionAttribute} kWh, which is larger than 2000 kWh!
</body>
</html>
