<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 24/03/2020
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error page</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<div class="alert-warning">
    '${ATTRIBUTE_AGE}' is not a valid input value for age. Please enter a non-negative integer number.
</div>
</body>
</html>
