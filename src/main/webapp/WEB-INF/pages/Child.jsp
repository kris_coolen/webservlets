<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 24/03/2020
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Child</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
You are a child: your age is ${ATTRIBUTE_AGE}, which is between 0 and 9.
</body>
</html>
