<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 29/03/2020
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/mytaglib.tld" prefix="mtl" %>
<html>
<head>
    <title>JSP tag with dynamic attributes</title>
    <%@ include file="/common/jsps/head.jspf" %>
</head>
<body>
<div class="w-25 p-3">
    <table class="table">
        <thead style="background-color: #0b2e13">
        <tr>
            <td style="color:white"><strong>color</strong></td>
            <td style="color:white"><strong>'hello world'</strong></td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>blue</td>
            <td><mtl:helloColor style="color:blue"/></td>
        </tr>
        <tr>
            <td>red</td>
            <td><mtl:helloColor style="color:red"/></td>
        </tr>
        <tr>
            <td>green</td>
            <td><mtl:helloColor style="color:green"/></td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
