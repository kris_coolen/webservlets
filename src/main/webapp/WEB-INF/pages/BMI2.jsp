<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 12/03/2020
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>BMI calculator</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<div align="right">
    <table class="table-borderless">
        <tbody>
        <tr>
            <td>Total visitors</td>
            <td>${visitorsTotal2}</td>
        </tr>
        <tr>
            <td>Active visitors</td>
            <td>${visitorsActive2}</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="container-fluid">
    <h1 class="align-content-center">BMI calculator</h1>
    <form class="was-validated" method="post">
        <div class="form-row">
            <div class="col-lg-2">
            <label for="validationHeight">Height </label>
            <input class="form-control" id = "validationHeight" type="text" name = "height" value="${param.height}" placeholder="required" pattern="[0,1,2]{1}\.[0-9]{2}" required  >
            <div class="valid-feedback">valid!</div>
            <div class="invalid-feedback">please use correct format</div>
            <small>Format: x.yy with x a number greater than zero, and y between 0 and 9</small>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-2">
            <label for="inputWeight">Weight </label>
            <input type="text" name = "weight" value="${param.weight}" class="form-control" id="inputWeight"
                   pattern="[1-9]{1}[0-9]*\.[0-9]{1}" placeholder="required" required>
            <div class="valid-feedback">valid!</div>
            <div class="invalid-feedback">Please use correct format</div>
            <small>Format: x.y with x a number greater than zero, and y between 0 and 9</small>
            </div>
        </div>
        <input type="submit" value="calculate" class="btn-primary"/><br>

        your bmi:
        <span style="color:${sessionScope.bmi>25? "RED": "GREEN"};">
            <fmt:formatNumber type = "number" maxFractionDigits="2" minFractionDigits="2" value ="${sessionScope.bmi}"/></span>
        <%--        <span style="color:${bmi>25? "RED": "GREEN"};">${bmi}--%>
    </form>
</div>
</body>
</html>

