<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 10/03/2020
  Time: 15:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Address Result</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<main>
    <h1>Address Result</h1>
    <article>
    Hello ${addressBean.firstName} ${addressBean.name}, you are successfully registered!<br>
    Please verify if all your personal information is correct:
    </article>
<table class="table" style="max-width:500px">
    <tbody>
    <tr>
        <td>Name</td>
        <td>${addressBean.name}</td>
    </tr>
    <tr>
        <td>First name</td>
        <td>${addressBean.firstName}</td>
    </tr>
    <tr>
        <td>Street</td>
        <td>${addressBean.street}</td>
    </tr>
    <tr>
        <td>Number</td>
        <td>${addressBean.number}</td>
    </tr>
    <tr>
        <td>Zip code</td>
        <td>${addressBean.zipCode}</td>
    </tr>
    <tr>
        <td>City</td>
        <td>${addressBean.city}</td>
    </tr>
    <tr>
        <td>Country</td>
        <td>${addressBean.country}</td>
    </tr>
    <tr>
        <td>Telephone number</td>
        <td>${addressBean.telephoneNumber}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>${addressBean.email}</td>
    </tr>

    </tbody>
</table>
</main>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>
