<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 30/03/2020
  Time: 22:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mtl" uri="/WEB-INF/mytaglib.tld"%>
<html>
<head>
    <title>To uppercase tag</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<mtl:uppercase>
    Deze tekst zal in hoofdletters worden afgedrukt.
</mtl:uppercase>
</body>
</html>
