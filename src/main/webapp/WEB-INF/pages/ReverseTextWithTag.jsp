<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 29/03/2020
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/mytaglib.tld" prefix="mtl"%>
<html>
<head>
    <title>Reverse text with tag attribute form</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<h1>Reverse text with tags form</h1>
<form method="post">
    <input type="text" name="text">
    <input type="submit" value="Reverse">
</form>
<mtl:reverse text="${param.text}"/>
</body>
</html>
