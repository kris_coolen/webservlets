<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 30/03/2020
  Time: 21:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mtl" uri="/WEB-INF/mytaglib.tld"%>
<html>
<head>
    <title>Tag with body</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
    <mtl:format style='color:red;font-weight:bold'>
        Deze tekst wordt geformatteerd (rood en vet).
    </mtl:format>
</body>
</html>
