<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 10/03/2020
  Time: 15:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Address Form</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<main>
    <div class="container-fluid">
    <h1 class="align-content-center">Address Form </h1>
        <form class="was-validated" method="POST">
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputName">Name </label>
                        <input type="text" name = "name" class="form-control" id="inputName" value="${addressBean.name}" placeholder="Enter your name" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputFirstName">First name </label>
                        <input type="text" name = "firstName" class="form-control" id="inputFirstName" value="${addressBean.firstName}" placeholder="Enter your first name" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputStreet">Street </label>
                        <input type="text" name = "street" class="form-control" id="inputStreet" value="${addressBean.street}" placeholder="Enter your street" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputNumber">Number </label>
                        <input type="text" name = "number" class="form-control" id="inputNumber" value="${addressBean.number}" placeholder="Enter your number" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputZipCode">Zip code </label>
                        <input type="text" name = "zipCode" class="form-control" id="inputZipCode" value="${addressBean.zipCode}" placeholder="Enter your zip code" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputCity">City </label>
                        <input type="text" name = "city" class="form-control" id="inputCity" value="${addressBean.city}" placeholder="Enter your city" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputCountry">Country </label>
                        <input type="text" name = "country" class="form-control" id="inputCountry" value="${addressBean.country}" placeholder="Enter your country" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputEmail">Email </label>
                        <input type="email" name = "email" class="form-control" id="inputEmail" value="${addressBean.email}" placeholder="Enter your email" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputTelephoneNumber">Telephone number </label>
                        <input type="tel" name = "telephoneNumber" class="form-control" id="inputTelephoneNumber" value="${addressBean.telephoneNumber}"
                               placeholder="Enter your telephone number" pattern="04[0-9]{2}/[0-9]{2}.[0-9]{2}.[0-9]{2}" required>
                        <div class="valid-feedback">Valid.</div>
                        <div class="invalid-feedback">Please fill out this field.</div>
                        <small>Format: 04xx/xx.xx.xx with x a number between 0 and 9</small>
                    </div>
                </div>
            </div>
            <input type="submit" value="Submit" class="btn-primary"/><br>
        </form>
    </div>
</main>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>
