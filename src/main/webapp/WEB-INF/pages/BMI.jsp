<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 12/03/2020
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>BMI calculator</title>
    <%@ include file="/common/jsps/head.jspf" %>
</head>
<body>

<div class="container-fluid">
    <div align="right">
        <table class="table-borderless">
            <tbody>
            <tr>
                <td> Total visitors: </td>
                <td>${visitorsTotal2}</td>
            </tr>
            <tr>
                <td> Active visitors: </td>
                <td>${visitorsActive2}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div algin="center">
        <form class="was-validated" method="post">
            <h1>BMI calculator</h1>
            <div class="form-row">
                <div class="col-lg-4">
                    <label for="inputHeight">Height </label>
                    <input type="text" name="height" class="form-control" id="inputHeight"
                           pattern="[0,1,2]{1}\.[0-9]{2}" placeholder="Enter your height (in m)" required>
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                    <small>Format: x.yy with x a number greater than zero, and y between 0 and 9</small>
                </div>

                <div class="col-lg-4">
                    <label for="inputWeight">Weight </label>
                    <input type="text" name="weight" class="form-control" id="inputWeight"
                           pattern="[1-9]{1}[0-9]*\.[0-9]{1}" placeholder="Enter your weight (in kg)" required>
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                    <small>Format: x.y with x a number greater than zero, and y between 0 and 9</small>
                </div>
            </div>
            <input type="submit" value="calculate" class="btn-primary"/><br>

            your bmi:
            <span style="color:${sessionScope.bmi>25? "RED": "GREEN"};">${sessionScope.bmi}</span>
            <%--        <span style="color:${bmi>25? "RED": "GREEN"};">${bmi}--%>
        </form>
    </div>
</div>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>
