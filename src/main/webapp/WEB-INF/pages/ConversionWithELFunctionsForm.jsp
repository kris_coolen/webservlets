<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 13/04/2020
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mtl" uri="/WEB-INF/mytaglib.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>EL Functions</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
    <form method="post">
        <input type="number" name="PARAM_KW"/> kw
        <input type="submit" value="to hp"/>
        <c:if test="${param.PARAM_KW!=null && param.PARAM_KW!=0}">
            ${param.PARAM_KW} kw = ${mtl:round(mtl:kw2hp(param.PARAM_KW),2)} hp
        </c:if>
        <br>
        <input type="number" name="PARAM_KM"/> km
        <input type="submit" value="to mile"/>
        <c:if test="${param.PARAM_KM!=null && param.PARAM_KM!=0}">
            ${param.PARAM_KM} km = ${mtl:round(mtl:km2mile(param.PARAM_KM),2)} mile
        </c:if>
    </form>
</body>
</html>
