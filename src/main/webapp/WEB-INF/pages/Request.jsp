<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 20/03/2020
  Time: 9:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Show Request Info</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<main>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1>Request info</h1>
                <table class="table-striped">
                    <thead>
                    <tr class="row-cols-2">
                        <th class="col-sm-2">  </th>
                        <th class="col-sm-2"> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="row-cols-2">
                        <td >http-method</td>
                        <td>${requestBean.httpMethod}</td>
                    </tr>
                    <tr class="row-cols-2">
                        <td>URI</td>
                        <td>${requestBean.uri}</td>
                    </tr>
                    <tr class="row-cols-2">
                        <td>URL</td>
                        <td>${requestBean.url}</td>
                    </tr>
                    <tr class="row-cols-2">
                        <td>Servlet Path</td>
                        <td>${requestBean.servletPath}</td>
                    </tr>
                    <tr class="row-cols-2">
                        <td>Context Path</td>
                        <td>${requestBean.contextPath}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</main>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>
