<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 13/04/2020
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
    <c:when test="${param.unit=='inch'}">
        <c:set var="factor" value="${param.PARAM_METER}"
    </c:when>
</c:choose>
<c:set var="unit" value="${param.PARAM_METER/factor}"/>
<html>
<head>
    <title>Metric Convertor 2</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<form>
    <input type="text" name="PARAM_METER" value="${param.PARAM_METER}"/> meter
    <input type="submit" value="="/>
    <input  type="text" disabled="disabled" value="${inch}"/>
    <select name="unit">
        <option value="inch">inch</option>
        <option value="feet">feet</option>
        <option value="yard">yard</option>
        <option value="mile">mile</option>
    </select>
</form>
</body>
</html>
<option value="0.0254">inch</option>
<option value="0.3048">feet</option>
<option value="0.9144">yard</option>
<option value="1609.34"
