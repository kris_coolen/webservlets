<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 22/03/2020
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Visitors</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<main>
    <div class="container">
        <div class="row justify-content-start">
            <div class="col-auto">
                <h3>Visitor data</h3>
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <td>Total visitors</td>
                        <td>Current visitors</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">${requestScope.totalVisitors}</td>
                        <td class="text-center">${requestScope.activeVisitors}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
</body>
</html>
