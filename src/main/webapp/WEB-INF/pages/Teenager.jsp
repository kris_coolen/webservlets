<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 24/03/2020
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Teenager</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
You are a teenager: your age is ${ATTRIBUTE_AGE}, which is between 10 and 19.
</body>
</html>
