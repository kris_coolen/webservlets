<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 30/03/2020
  Time: 22:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mtl" uri="/WEB-INF/mytaglib.tld"%>
<html>
<head>
    <title>Repeat text with tag</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
    <mtl:repeat count="5">
        Deze zin wordt 5 maal herhaald.
    </mtl:repeat>
</body>
</html>
