<%@ page import="be.kriscoolen.hoofdstuk3.BeerExercise.domain.Beer" %>
<%@ page import="java.util.List" %>

<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 20/03/2020
  Time: 12:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Beer Catalog</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<%
    List<Beer> beerList = (List<Beer>)request.getAttribute("beerList");
%>

<%if (beerList!=null){%>
    <h2>List of requested beers</h2>
    <table>
        <thead>
        <tr>
            <th>Brand</th>
            <th>Alcohol</th>
        </tr>
        </thead>
        <tbody>
        <% for(Beer b: beerList) {%>
        <tr>
            <td><%=b.getBrand()%></td>
            <td><%=b.getAlcoholPercentage()%></td>
        </tr>
        <%
        };
        %>
        </tbody>
    </table>
<% }else{ %>
<a>Please Select a beer type!</a>
<%} %>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>
