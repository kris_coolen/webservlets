<%@ page import="java.util.Properties" %><%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 23/03/2020
  Time: 8:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Properties properties = (Properties) request.getAttribute("propMariaDB");
%>
<html>
<head>
    <title>Resource Servlet</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<main>
    <div class="mt-2 container-fluid">
        <div class="row justify-content-center">
            <div class="col-auto">
               <table class="table">
                <thead class="thead-dark">
                <tr class=row-cols-2">
                    <th >Name</th>
                    <th >Value</th>
                </tr>
                </thead>
                <tbody>
                <tr class="row-cols-2">
                    <td > driver </td>
                    <td ><%=properties.getProperty("driver")%></td>
                </tr>
                <tr class ="row-cols-2">
                    <td > url </td>
                    <td ><%=properties.getProperty("url")%></td>
                </tr>
                <tr class="row-cols-2">
                    <td > user </td>
                    <td ><%=properties.getProperty("user")%></td>
                </tr>
                <tr class="row-cols-2">
                    <td > password </td>
                    <td ><%=properties.getProperty("password")%></td>
                </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</main>
</body>
</html>
