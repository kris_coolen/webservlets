<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 1/04/2020
  Time: 8:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tf" tagdir="/WEB-INF/tags" %>
<html>
<head>
    <title>format text with tag file</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
    <tf:textFormatColorSizeTag color="blue" size="20">
        Deze tekst verschijnt in blauw in grote letters.
    </tf:textFormatColorSizeTag>
</body>
</html>
