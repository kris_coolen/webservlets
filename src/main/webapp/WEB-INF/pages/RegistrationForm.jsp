<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 20/03/2020
  Time: 10:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration Form</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<%--<%@ include file="/common/jsps/header.jspf"%>--%>
<main>
    <div class="mx-auto" style="width: 640px;">
        <h1 class="align-content-center">Registration Form</h1>
        <form class="was-validated bg-light"  method="post" >
            <div class="form-group">
                <label for="inputNameId">Name </label>
                <input type="text" name = "inputName" class="form-control" id="inputNameId" required>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please fill out this field.</div>
            </div>
            <div class="form-group">
                <label for="inputPassWordId">Password </label>
                <input type="password" name = "inputPassword" class="form-control" id="inputPassWordId"
                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                       title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                       required>
                <div class="valid-feedback">Valid password</div>
                <div class="invalid-feedback">Password must contain at least one number, one uppercase and lowercase letter, and at least 8 characters.</div>
            </div>
            <input type="submit" value="register" class="btn-primary"/><br>
        </form>
    </div>
</main>
<%--<%@ include file="/common/jsps/footer.jspf"%>--%>
</body>
</html>
