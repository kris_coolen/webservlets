<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 13/04/2020
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="factor" value="0.0254"/>
<c:set var="inch" value="${param.PARAM_METER/factor}"/>
<html>
<head>
    <title>Metric Convertor</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
<form>
    <input type="text" name="PARAM_METER" value="${param.PARAM_METER}"/> meter
    <input type="submit" value="="/>
    <input  type="text" disabled="disabled" value="${inch}"/> inch
</form>
</body>
</html>
