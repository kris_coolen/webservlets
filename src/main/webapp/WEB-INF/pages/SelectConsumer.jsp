<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Select consumer</title>
    <%@ include file="/common/jsps/head.jspf" %>
</head>
<body>
<main>
    <div class="mt-2 container-fluid">
        <form class="was-validated">
            <div class="form-group">
                <label for="inputConsumption">Energy consumption(kWh/year) </label>
                <input type = "text" id="inputConsumption" name="consumption">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</main>
</body>
</html>