<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 29/03/2020
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/mytaglib.tld" prefix="mtl"%>
<html>
<head>
    <title>Use of JSP tags</title>
    <%@ include file="/common/jsps/head.jspf"%>
</head>
<body>
  <mtl:hello/>
</body>
</html>
