<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 25/03/2020
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Login Error</title>
    <%@ include file="/common/jsps/head.jspf" %>
</head>
<body>
<c:url var="url" value="/login.jsp"/>
<div class="error-container">
    <div class="error-content">
        <h2>Invalid user name or password.</h2>
        <img src="common/image/user-invalid-512.png" style="height:60%";/>
        <p>Please enter a user name or password that is authorized to access this
            application. Click here to <a href="${url}">Try Again</a></p>
    </div>
</div>
</body>
</html>
