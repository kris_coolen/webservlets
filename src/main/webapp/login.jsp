<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 25/03/2020
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Page</title>
    <%@ include file="/common/jsps/head.jspf" %>
</head>
<body>
    <div class="mt-5 container-fluid">
        <div class="row justify-content-center">
            <div class="w-25">
                <div class="table">
                    <h2>Hello, please log in:</h2>
                    <form action="j_security_check" method=post>
                        <table class="table-borderless table grid">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>username: </strong></td>
                                    <td colspan="2"><input type="text" name="j_username"></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>password: </strong></td>
                                    <td colspan="2"><input type="password" name="j_password"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"><input type="submit" value="Login"></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
